﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace blazor1.Services
{
    public class CounterService
    {
        public int Counter { get; set; } = 0;

        public void Increment()
        {
            Counter++;
        }
    }
}
